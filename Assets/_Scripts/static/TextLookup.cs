using System.Collections.Generic;

public static class TextLookup {
  public static Dictionary<int, string> RankText = new Dictionary<int, string>()
  {
    {1, "1st"},
    {2, "2nd"},
    {3, "3rd"},
    {4, "4th"}
  };
}
