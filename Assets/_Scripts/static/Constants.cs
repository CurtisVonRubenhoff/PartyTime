public static class Constants {
  public static float MOVE_TIME = 0.25f;
  public static float MOVE_DELAY = 0.016f;
  public static float PIECE_DISTANCE = 0.4f;
  public static int MAX_PLAYERS = 4;
  public static int PLAYER_START_CASH = 10;
}
